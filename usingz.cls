
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% usingz.cls: class file for Using Z - February 1999

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{usingz}[1999/22/02 Using Z Class]

\newif\if@@lucida@@fonts@@ \@@lucida@@fonts@@false
\newif\if@@using@@color@@ \@@using@@color@@false
\newif\if@@monochrome@@ \@@monochrome@@false
\newif\if@@twoup@@ \@@twoup@@false

\DeclareOption{lucida}{\@@lucida@@fonts@@true}
\DeclareOption{cm}{\@@lucida@@fonts@@false}
\DeclareOption{color}{\@@using@@color@@true}
\DeclareOption{monochrome}{\@@monochrome@@true}
\DeclareOption{twoup}{\@@twoup@@true}

\ProcessOptions

\LoadClass[fleqn,openright]{book}

\if@@lucida@@fonts@@
  \RequirePackage{zed-lbr}
  \font\FontA = lbd at 45pt %
  \font\FontB = lbr at 15pt %
  \font\FontC = lbd at 18pt
\else
  \RequirePackage{zed-cm}
  \font\FontA = cmr10 at 45pt %
  \font\FontB = cmr10 at 15pt %
  \font\FontC = cmr10 at 18pt
\fi 

\RequirePackage{zed-float}
\RequirePackage{zed-text}
\RequirePackage{zed-proof}
\RequirePackage{zed-maths}

\if@@using@@color@@
  \if@@monochrome@@
    \RequirePackage[monochrome]{color}
  \else
    \RequirePackage{color}
  \fi
  \RequirePackage{crayola}
  \RequirePackage{zed-floatc}
  \RequirePackage{zed-textc}
  \RequirePackage{zed-proofc}
  \RequirePackage{zed-mathsc}
\fi

\if@@twoup@@
  \AtBeginDocument{%
    \input 2up.tex \special{landscape}
    \source{1000}{210mm}{297mm} \target{900}{297mm}{210mm}
    }
\fi

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\endinput

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
@


1.1
log
@*** empty log message ***
@
text
@@

