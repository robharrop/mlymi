\documentclass{beamer}

% Allow graphics to be included from the images directory
\usepackage{graphicx}
\DeclareGraphicsExtensions{.pdf,.png,.jpg}
\graphicspath{ {images/} }

% Set Z Notation
\usepackage{zed-cm}

\title{Model Like You Mean It}
\author{Rob Harrop}

\begin{document}
\frame{\titlepage}

\begin{frame}
\frametitle{Agenda}
\begin{itemize}
  \item Why Model?
  \item Modelling Languages
  \item Formal Modelling (really!)
  \item Modelling State and Behaviour with Z
  \item Modelling Concurrency with CSP
  \item What Next?
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Why Model?}
\begin{itemize}
	\item{Design}
	\item{Collaborate}
	\item{Communicate}
	\item{Comprehend}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Modelling \textbf{is not:}}
\begin{itemize}
	\item Documenting
	\item Mandatory
	\item Implementing
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Modelling \textbf{is:}}
\begin{itemize}
	\item Agile
	\item Lightweight
	\item Valuable
	\item Tactical
	\item Diverse
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Modelling Languages}
\begin{itemize}
	\item Unified Modelling Language (UML)
	\item Object Role Modelling (ORM)
	\item Entity-Relationship Diagramming (ERD)
	\item Z
	\item CSP
	\item Alloy
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{UML}
\includegraphics[scale=0.3]{sequence}
\end{frame}

\begin{frame}
\frametitle{ORM}
\includegraphics[scale=0.5]{orm}
\end{frame}

\begin{frame}
\frametitle{ERD}
\includegraphics[scale=0.3]{erd}
\end{frame}

\begin{frame}
\frametitle{Why Formal Modelling?}
\begin{itemize}
	\item Precise
	\item Powerful
	\item Provable (maybe?)
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Why \textbf{not} Formal Modelling?}
\begin{itemize}
	\item Heavyweight?
	\item Complex?
	\item Overkill?
	\item Too much maths?
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Z Notation}
\begin{itemize}
	\item Created in 1974 by Jean-Raymond Abrial
	\item Draws from set theory, lambda calculus and first-order predicate logic
	\item Light-touch modelling
	\item Broad proof support
	\item Excellent choice for modelling state and behaviour
	\item Will be very familiar to functional programmers
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Communicating Sequential Processes}
\begin{itemize}
	\item Created in 1978 by Tony Hoare
	\item Perfect for modelling concurrency
	\item Excellent tool support
	\item Inspired Erlang, Akka, Go and more
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Alloy}
\end{frame}

\begin{frame}
\frametitle{Modelling a Message Broker}
\begin{itemize}
	\item \textbf{Messages} are stored in \textbf{Queues}
	\item \textbf{Publishers} enqueue \textbf{Messages}
	\item \textbf{Consumers} dequeue \textbf{Messages}
	\item \textbf{Messages} are delivered to at most one \textbf{Consumer}
	\item \textbf{Queues} are FIFO
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Messages}
\begin{zed}
[Message]
\end{zed}
\end{frame}

\begin{frame}
\frametitle{Messages}
\begin{schema}{Message}
id: MessageId \\
body: MessageBody
\end{schema}
\end{frame}

\begin{frame}
\frametitle{Queues}
\begin{schema}{Queue}
messages: \power Message \\
\end{schema}
\end{frame}

\begin{frame}
\frametitle{Queues}
\begin{schema}{Queue}
messages: \seq Message \\
\end{schema}
\end{frame}

\begin{frame}
\frametitle{Relations}
Binary relation between $X$ and $Y$
\begin{zed}
X \rel Y == \power (X \cross Y)
\end{zed}
\end{frame}

\begin{frame}
\frametitle{Functions}
Partial functions
\begin{zed}
X \pfun Y  ==
        \{~f: X \rel Y | (\forall x: X; y_1, y_2: Y @ \\
\t4	(x \mapsto y_1) \in f \land (x \mapsto y_2) \in f %
                                                  \implies y_1 = y_2)~\} \\
\end{zed}

Finite partial functions
\begin{zed}
X \ffun Y  ==  \{~f: X \pfun Y | \dom f \in \finset X~\}
\end{zed}
\end{frame}

\begin{frame}
\frametitle{Sequences}
\begin{zed}
\seq X == \{f: \nat \ffun X | \dom f = 1 \upto \#f\}
\end{zed}
\begin{zed}
ms == \{1 \mapsto m_1, 2 \mapsto m_2 \}\\
ms~1 = m_1 
\end{zed}
\end{frame}

\begin{frame}
\frametitle{Queues}
\begin{schema}{Queue}
messages: \seq Message \\
\where
messages \subseteq (\nat \finj Message) \\
\end{schema}
\end{frame}

\begin{frame}
\frametitle{Queues}
\begin{schema}{Queue}
messages: \iseq Message \\
\end{schema}
\end{frame}

\begin{frame}
\frametitle{Queue Length}
\begin{schema}{QueueLength}
\Xi Queue \\
len!: \nat \\
\where
len! = \# messages
\end{schema}
\end{frame}

\begin{frame}
\frametitle{Enqueueing a Message}
\begin{schema}{Enqueue}
\Delta Queue \\
m?: Message \\
\where
messages' = messages \cat \langle m? \rangle \\
\end{schema}
\end{frame}

\begin{frame}
\frametitle{Dequeueing a Message}
\begin{schema}{Dequeue}
\Delta Queue \\
m!: Message \\
\where
messages' = tail~messages \\
m! = head~messages \\
\end{schema}
\end{frame}

\begin{frame}
\frametitle{Dequeueing a Message}
\begin{zed}
Result ::= ok | error \\
\end{zed}
\end{frame}

\begin{frame}
\frametitle{Dequeueing a Message}
\begin{schema}{DequeueSuccess}
\Delta Queue \\
m!: Message \\
r!: Result \\
\where
messages \neq \emptyset \\
messages' = tail~messages \\
m! = head~messages \\
r! = ok \\
\end{schema}
\end{frame}

\begin{frame}
\frametitle{Dequeueing a Message}
\begin{schema}{DequeueEmpty}
\Delta Queue \\
m!: Message \\
r!: Result \\
\where
r! = empty \\
\end{schema}
\end{frame}

\begin{frame}
\frametitle{Dequeueing a Message}
\begin{zed}
Maybe ::= Nothing | Just \ldata Message \rdata \\
\end{zed}
\end{frame}

\begin{frame}
\frametitle{Dequeueing a Message}
\begin{schema}{DequeueSuccess}
\Delta Queue \\
r!: Maybe \\
\where
messages \neq \emptyset \\
messages' = tail~messages \\
r! = Just~(head~messages) \\
\end{schema}
\end{frame}

\begin{frame}
\frametitle{Dequeueing a Message}
\begin{schema}{DequeueEmpty}
\Delta Queue \\
r!: Maybe \\
\where
r! = Nothing \\
\end{schema}
\end{frame}

\begin{frame}
\frametitle{Dequeueing}
\begin{zed}
Dequeue \defs DequeueSuccess \lor DequeueEmpty
\end{zed}
\end{frame}

\begin{frame}
\frametitle{Broker}
\begin{schema}{Broker}
queues: QueueName \pinj Queue \\
\end{schema}
\end{frame}

\begin{frame}
\frametitle{Publishing Messages}
\begin{schema}{Publish}
\Delta Broker \\
m?: Message \\
q?: QueueName \\
\where
q? \in \dom queues \\
(queues'~q?).messages = (queues~q?) \cat \langle m? \rangle\\
\end{schema}
\end{frame}

\begin{frame}
\frametitle{Publishing Messages}
\begin{schema}{Publish}
\Delta Broker \\
m?: Message \\
q?: QueueName \\
\where
q? \in \dom queues \\
\{q?\} \ndres queues' = \{q?\} \ndres queues \\
(queues'~q?).messages = (queues~q?) \cat \langle m? \rangle\\
\end{schema}
\end{frame}

\begin{frame}
\frametitle{Promotion}
\begin{schema}{PromoteQueue}
\Delta Broker \\
\Delta Queue \\
q?: QueueName \\
\where
q? \in \dom queues\\
\theta Queue = queues~q? \\
queues' = queues \oplus \{q? \mapsto \theta Queue'\} \\
\end{schema}
\end{frame}

\begin{frame}
\frametitle{Promotion}
\begin{zed}
Publish \defs \exists \Delta Queue @ PromoteQueue \land Enqueue
\end{zed}
\end{frame}

\begin{frame}
\frametitle{Non-determinism}
\end{frame}


\end{document}